import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomfooterComponent } from './customfooter/customfooter/customfooter.component';
var CustomfootermoduleModule = /** @class */ (function () {
    function CustomfootermoduleModule() {
    }
    CustomfootermoduleModule = tslib_1.__decorate([
        NgModule({
            declarations: [CustomfooterComponent],
            imports: [
                CommonModule
            ]
        })
    ], CustomfootermoduleModule);
    return CustomfootermoduleModule;
}());
export { CustomfootermoduleModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tZm9vdGVybW9kdWxlLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXJlbGVtZW50cy8iLCJzb3VyY2VzIjpbImN1c3RvbWZvb3Rlcm1vZHVsZS9jdXN0b21mb290ZXJtb2R1bGUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxvREFBb0QsQ0FBQztBQVUzRjtJQUFBO0lBQXdDLENBQUM7SUFBNUIsd0JBQXdCO1FBTnBDLFFBQVEsQ0FBQztZQUNSLFlBQVksRUFBRSxDQUFDLHFCQUFxQixDQUFDO1lBQ3JDLE9BQU8sRUFBRTtnQkFDUCxZQUFZO2FBQ2I7U0FDRixDQUFDO09BQ1csd0JBQXdCLENBQUk7SUFBRCwrQkFBQztDQUFBLEFBQXpDLElBQXlDO1NBQTVCLHdCQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgQ3VzdG9tZm9vdGVyQ29tcG9uZW50IH0gZnJvbSAnLi9jdXN0b21mb290ZXIvY3VzdG9tZm9vdGVyL2N1c3RvbWZvb3Rlci5jb21wb25lbnQnO1xuXG5cblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbQ3VzdG9tZm9vdGVyQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIEN1c3RvbWZvb3Rlcm1vZHVsZU1vZHVsZSB7IH1cbiJdfQ==