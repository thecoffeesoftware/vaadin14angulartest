import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var CustomfooterComponent = /** @class */ (function () {
    function CustomfooterComponent() {
    }
    CustomfooterComponent.prototype.ngOnInit = function () {
    };
    CustomfooterComponent = tslib_1.__decorate([
        Component({
            selector: 'test-customfooter',
            template: "<p>customfooter works!</p>\n",
            styles: [""]
        })
    ], CustomfooterComponent);
    return CustomfooterComponent;
}());
export { CustomfooterComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tZm9vdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXJlbGVtZW50cy8iLCJzb3VyY2VzIjpbImN1c3RvbWZvb3Rlcm1vZHVsZS9jdXN0b21mb290ZXIvY3VzdG9tZm9vdGVyL2N1c3RvbWZvb3Rlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFPbEQ7SUFFRTtJQUFnQixDQUFDO0lBRWpCLHdDQUFRLEdBQVI7SUFDQSxDQUFDO0lBTFUscUJBQXFCO1FBTGpDLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxtQkFBbUI7WUFDN0Isd0NBQTRDOztTQUU3QyxDQUFDO09BQ1cscUJBQXFCLENBT2pDO0lBQUQsNEJBQUM7Q0FBQSxBQVBELElBT0M7U0FQWSxxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd0ZXN0LWN1c3RvbWZvb3RlcicsXG4gIHRlbXBsYXRlVXJsOiAnLi9jdXN0b21mb290ZXIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9jdXN0b21mb290ZXIuY29tcG9uZW50LmNzcyddXG59KVxuZXhwb3J0IGNsYXNzIEN1c3RvbWZvb3RlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG59XG4iXX0=