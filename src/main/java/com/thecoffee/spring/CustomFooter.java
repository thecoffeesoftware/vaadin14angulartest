package com.thecoffee.spring;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.JavaScript;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.shared.ui.LoadMode;

@Tag(value = "test-customfooter")
@JsModule(value = "src/customfootermodule/customfootermodule.module.js", loadMode = LoadMode.EAGER)
@JavaScript(value = "src/customfootermodule/customfooter/customfooter/customfooter.component.js", loadMode = LoadMode.EAGER)
public class CustomFooter extends Component {

	public CustomFooter() {
	}

}
